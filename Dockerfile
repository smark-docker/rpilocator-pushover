FROM python:3-slim

ENV PYTHONUNBUFFERED 1

RUN pip install requests feedparser

COPY rpilocator-pushover.py /app/

CMD ["/app/rpilocator-pushover.py"]
