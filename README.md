# rpilocator Pushover

Notify about rpilocator updates via pushover

Stolen from https://github.com/camerahacks/rpilocator-rss-feed/

## Variables

| Name               | Description               | Example                 |
|:------------------ |:------------------------- |:----------------------- |
| `FEED_FILTER`      | Additional URL Parameters | country=DE&cat=CM4      |
| `PUSHOVER_KEY`     | User Key                  | x9Pt1zGOU31OJH9Osv18B   |
| `PUSHOVER_API_KEY` | Application Key           | aqt3hatxphf952mt7nbq8vm |
