#! /usr/bin/env python

import time
import os
import sys
import requests
import feedparser


def die(msg=""):
    sys.stderr.write(msg + "\n")
    sys.exit(1)


# Create the message body
def formatMessage(entry):
    messageData = (
        "token="
        + PUSHOVER_API_KEY
        + "&user="
        + PUSHOVER_KEY
        + "&title="
        + MESSAGE_TITLE
    )
    return messageData + "&message=" + entry.title + "&url=" + entry.link


# Send the push/message to all devices connected to Pushbullet
def sendMessage(message):
    try:
        requests.post(
            url="https://api.pushover.net/1/messages.json", data=message, timeout=20
        )
    except requests.exceptions.Timeout:
        print("Request Timeout")
    except requests.exceptions.TooManyRedirects:
        print("Too many requests")
    except requests.exceptions.RequestException as e:
        print(e)


FEED_URL = "https://rpilocator.com/feed/?" + str(os.getenv("FEED_FILTER", ""))
PUSHOVER_KEY = os.getenv("PUSHOVER_KEY") or die("PUSHOVER_KEY not defined")
PUSHOVER_API_KEY = os.getenv("PUSHOVER_API_KEY") or die("PUSHOVER_API_KEY not defined")
MESSAGE_TITLE = "xlocator Stock Alert"
USER_AGENT = "xlocator feed alert"

# Set control to blank list
control = []

# Fetch the feed
f = feedparser.parse(FEED_URL, agent=USER_AGENT)

# If there are entries in the feed, add entry guid to the control variable
if f.entries:
    for entries in f.entries:
        control.append(entries.id)

print("starting main loop")
time.sleep(60)
while True:
    # Fetch the feed again, and again, and again...
    f = feedparser.parse(FEED_URL, agent=USER_AGENT)

    # Compare feed entries to control list.
    # If there are new entries, send a message/push
    # and add the new entry to control variable
    for entries in f.entries:
        if entries.id not in control:

            message = formatMessage(entries)

            sendMessage(message)

            # Add entry guid to the control variable
            control.append(entries.id)

    time.sleep(60)
